FROM mhart/alpine-node:7.2.0

ENV base /srv/go-agent
WORKDIR ${base}
CMD ["./agent.sh"]

RUN apk update &&\
    apk add docker git openssh-client subversion mercurial openjdk8-jre bash python make g++ &&\
    npm install -g yarn docker-snapshot-image


ENV rev 3819
ENV version 16.7.0

ADD ./go-agent-config /etc/default/go-agent

ADD https://download.go.cd/binaries/${version}-${rev}/generic/go-agent-${version}-${rev}.zip /tmp/go-agent.zip

RUN cd /tmp &&\
    unzip ./go-agent.zip &&\
    rm -rf ${base} &&\
    mv ./go-agent-${version} ${base} &&\
    chmod 700 ${base}/agent.sh &&\
    rm -rf /tmp/*



